A numerical simulation of ASC. 

###########################################################
OVERVIEW:

In this simulation we included only DRMI: i.e. 
PRM, BS, ITMX, ITMY, SRM
In the simulation we assume that all the length and centering loops are working, so we only consider misalignment signals. Carrier is neglected in current version because we are focusing on AS 36 signal.

All the core function goes in AS36num_vII.py

############################################################
PROJECT 1:
AS36_tilt_only_v01.ipynb

In this notebook we focus on studying the stability of AS 36 error signal against differential/common thermal lensing effects.

We misalign either BS or SRM for a fixed normalized dim-less angle, and then vary the ROC of ITMs by some dim-less amount, either differentially or commonly. 

The conclusions are:
 # AS 36 WFSB Q phase is a good sensor for BS signal
   good seperation from SRM signal
   very stable against common dRoC
   and enough for diff. dRoC
 # Cannot use only WFSB to control both BS (Q-phase) and SRM (I-phase)
   SRM signal unstable against diff. dRoc
 # Using WFSA for SRM, on the other hand, can work pretty well
