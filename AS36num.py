# -*- coding: utf-8 -*-
"""
AS36num.py
AS36 signal in a pure numerical way
"""

import numpy as np
import scipy.constants as scc

c = scc.c # speed of light in [m/s]
lambda_0 = 1064 * 1.e-9 # laser wavelength in [m]
k_0 = 2.*np.pi/lambda_0 # wave number
omega_0 = k_0 * c # angular velocity of the laser


lsch=0.08
lp=57.65
ls=56.0

n0=1.45 # refractive indx of fused sillica
Ritm=-1934./n0 # ROC of ITM seen from mich side
Rsr3=36. # ROC of SR3

rx=-1.0 # reflection of ITMX 
ry=-1.0
tp=np.sqrt(0.031)
rp=-np.sqrt(1.-tp**2.)
ts=np.sqrt(0.37)
rs=-np.sqrt(1.-ts**2.)

freq1=9.e6
freq2=45.e6

rM1=np.cos(lsch*2.*np.pi*freq1/c)
rM2=np.cos(lsch*2.*np.pi*freq2/c)
tM1=np.sin(lsch*2.*np.pi*freq1/c)
tM2=np.sin(lsch*2.*np.pi*freq2/c)
rsm1=(rM1-rs)/(1.-rs*rM1)
tsm1=(ts*tM1)/(1.-rs*rM1)#np.sqrt(1.-rsm1**2.)
rsm2=(rM2+rs)/(1.+rs*rM2)
tsm2=(ts*tM2)/(1.+rs*rM2)#np.sqrt(1.-rsm2**2.

class SideBand:
    def __init__(self):
        pass
        
    def C(self, dRoc):
        """
        curvature mismatch matrix
        [00, 10, 20]' = C*[00, 10, 20]'
        """
        C=np.array([\
            [1.-1j*dRoc, 0., -1j*dRoc/np.sqrt(2)],\
            [0., 1.-2j*dRoc, 0.],\
            [-1j*dRoc/np.sqrt(2), 0., 1.-3j*dRoc]\
            ], dtype=np.complex)
        return C
        
    def M(self, Theta):
        """
        tilting matrix
        [00, 10, 20]'=M*[00, 10, 20]'
        """
        M=np.array([\
            [1., -2j*Theta, 0.],\
            [-2j*Theta, 1., -np.sqrt(8.)*1j*Theta],\
            [0., -np.sqrt(8.)*1j*Theta, 1.]\
            ], dtype=np.complex)
        return M
        
    def t_bs(self, omega, dRocx, dRocy, Theta):
        """
        trans. matrix for bs
        from PRCL to SRCL
        """
        t_bs=-0.5j*\
            (rx*np.dot(self.M(Theta), self.C(dRocx))*np.exp(1j*omega*lsch/c)\
            -ry*np.dot(self.C(dRocy), self.M(Theta))*np.exp(-1j*omega*lsch/c))
        return t_bs
        
    def r_bs(self, omega, dRocx, dRocy, Theta):
        """
        ref. matrix for bs
        from PRCL to SRCL
        r_bs>0 as rx=ry=-1
        """
        r_bs=-0.5*\
            (rx*self.C(dRocx)*np.exp(1j*omega*lsch/c)\
            +ry*self.C(dRocy)*np.exp(-1j*omega*lsch/c))
        return r_bs
        
    def t_bs2(self, omega, dRocx, dRocy, Theta):
        """
        trans. matrix for bs
        from SRCL to PRCL
        """
        t_bs2=-0.5j*\
            (rx*np.dot(self.C(dRocx), self.M(-Theta))*np.exp(1j*omega*lsch/c)\
            -ry*np.dot(self.M(-Theta), self.C(dRocy))*np.exp(-1j*omega*lsch/c))
        return t_bs2
        
    def r_bs2(self, omega, dRocx, dRocy, Theta):
        """
        ref. matrix for bs
        from SRCL to PRCL
        r_bs2<0 as rx=ry=-1
        """
        r_bs2=0.5*\
            (rx*self.C(dRocx)*np.exp(1j*omega*lsch/c)\
            +ry*self.C(dRocy)*np.exp(-1j*omega*lsch/c))
        return r_bs2
        
    def Psrcl(self, omega, eta):
        """
        single trip phase evolution in SRCL
        omega: angular mod. freq 
        eta: guoy phase shift
        for 45 MHz 00 Psrcl**2. = 1
             9 MHz 00 Psrcl**2. = -1
        """
        Psrcl=np.ones(3, dtype=np.complex)
        if np.abs(omega)<(2.*np.pi*10.e6): 
            # 9 MHz sb.
            # 00 anti-reson. in SRCL, single trip has a phase pi/2
            Psrcl[0]=1j
            Psrcl[1]=1j*np.exp(1j*eta)
            Psrcl[2]=1j*np.exp(2j*eta)
        else:
            # 45 MHz sb. 
            # 00 reson. in SRCL, single trip has a phase pi
            Psrcl[0]=-1.
            Psrcl[1]=-np.exp(1j*eta)
            Psrcl[2]=-np.exp(2j*eta)
            
        Psrcl=np.diag(Psrcl)
        return Psrcl
        
    def Pprcl(self, omega, eta):
        """
        single trip phase evolution in PRCL
        """
        Pprcl=-np.ones(3, dtype=np.complex)
        Pprcl[0]=1j
        Pprcl[1]=1j*np.exp(1j*eta)
        Pprcl[2]=1j*np.exp(2j*eta)
        
        Pprcl=np.diag(Pprcl)
        return Pprcl
        
    def Pdmd(self, eta):
        """
        accounting for the extra gouy phase 
        after transmitted from the cavity
        """
        Pdmd=np.ones(3, dtype=np.complex)
        Pdmd[1]=np.exp(1j*eta)
        Pdmd[2]=np.exp(2j*eta)
        return Pdmd
        
    def g_cav_sm(self, omega, eta, dRocx, dRocy, Theta):
        """
        gain matrix of the compound cavity formed by mich + src
        """
        r_bs2=self.r_bs2(omega, dRocx, dRocy, Theta) # r_bs2<0
        t_bs=self.t_bs(omega, dRocx, dRocy, Theta)
        Psrcl=self.Psrcl(omega, eta)
        
        g_cav=rs*np.dot(Psrcl, Psrcl)
        g_cav=np.dot(r_bs2, g_cav)
        g_cav=np.eye(3, dtype=np.complex)-g_cav
        g_cav=np.linalg.inv(g_cav)
        g_cav=np.dot(g_cav, t_bs)
        return g_cav
        
    def t_sm(self, omega, eta, dRocx, dRocy, Theta):
        """
        trans. matrix for signal-recycled Mich. 
        """
        Psrcl=self.Psrcl(omega, eta)        
        g_cav=self.g_cav_sm(omega, eta, dRocx, dRocy, Theta)        
        t_sm=ts*np.dot(Psrcl, g_cav)
        return t_sm
        
    def r_sm(self, omega, eta, dRocx, dRocy, Theta):
        """
        ref. matrix for signal-recycled mich
        """
        r_bs=self.r_bs(omega, dRocx, dRocy, Theta) # r_bs>0
        t_bs2=self.t_bs2(omega, dRocx, dRocy, Theta)
        Psrcl=self.Psrcl(omega, eta)
        g_cav=self.g_cav_sm(omega, eta, dRocx, dRocy, Theta)
        
        r_sm=rs*np.dot(Psrcl, Psrcl)
        r_sm=np.dot(r_sm, g_cav)
        r_sm=np.dot(t_bs2, r_sm)
        r_sm=r_bs + r_sm
        return r_sm
        
    def g_cav_dm(self, omega, eta, dRocx, dRocy, Theta, eta_P=2.08):
        """
        cavity gain (matrix) for dual-recycled Mich cavity
        eta = gouy phase in SRCL
        eta_P=gouy phase in PRCL
        """
        r_sm=self.r_sm(omega, eta, dRocx, dRocy, Theta)
        P=self.Pprcl(omega, eta_P)
        
        g_cav=np.dot(r_sm, P)
        g_cav=np.dot(P, g_cav)
        g_cav=np.eye(3, dtype=np.complex)-rp*g_cav
        g_cav=np.linalg.inv(g_cav)
        g_cav=tp*g_cav
        return g_cav
        
    def t_dm(self, omega, eta, dRocx, dRocy, Theta, eta_P=2.08):
        P=self.Pprcl(omega, eta_P)
        g_dm=self.g_cav_dm(omega, eta, dRocx, dRocy, Theta, eta_P)
        t_sm=self.t_sm(omega, eta, dRocx, dRocy, Theta)
        
        t_dm=np.dot(P, g_dm)
        t_dm=np.dot(t_sm, t_dm)
        return t_dm
        
    def r_dm(self, omega, eta, dRocx, dRocy, Theta, eta_P=2.08):
        P=self.Pprcl(omega, eta_P)
        g_dm=self.g_cav_dm(omega, eta, dRocx, dRocy, Theta, eta_P)
        r_sm=self.r_sm(omega, eta, dRocx, dRocy, Theta)
        
        r_dm=np.dot(P, g_dm)
        r_dm=np.dot(r_sm, r_dm)
        r_dm=np.dot(P, r_dm)
        r_dm=tp*r_dm
        r_dm=-rp*np.eye(3)+r_dm
        return r_dm
        
    def find_dmd_ang(self,E1,E2):
        """
        E1 and E2 both complex scalars, with f1 >= f2
        return dmd angle such that maximizes I-phase (cos)
        """
        ang1=np.angle(E1)
        ang2=np.angle(E2)
        d_ang=ang1-ang2
        return d_ang
        
    def BeatNote(self, E1, E2, dmd_ang):
        """
        Find the beat note between two E-fields 
        E1 and E2 both complex scalars, with f1 >= f2
        dmd_ang is the demodulation angle
        return power in both
        I-phase (cos)
        and Q-phase(sin)
        [P_I, P_Q]
        """
        E2=E2*np.exp(1j*dmd_ang)
        a1=np.real(E1)
        b1=np.imag(E1)
        a2=np.real(E2)
        b2=np.imag(E2)
        P=np.zeros(2)
        P[0]=a1*a2+b1*b2
        P[1]=a1*b2-a2*b1
        return P
        
