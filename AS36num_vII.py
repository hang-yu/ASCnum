# -*- coding: utf-8 -*-
"""
AS36num_vII.py
AS36 signal in a pure numerical way
centers of all optics are aligned in a same line
considering curvature mismatches of ITMs 
and misalignment of BS, SRM
"""

import numpy as np
import scipy.constants as scc

c = scc.c # speed of light in [m/s]
lambda_0 = 1064 * 1.e-9 # laser wavelength in [m]
k_0 = 2.*np.pi/lambda_0 # wave number
omega_0 = k_0 * c # angular velocity of the laser


#lsch = 0.095 # Schnupp asymmetry [m]
lsch = 0.08 # Schnupp asymmetry [m]
#lsch = 0.05
lp = 57.65 # Power recycling cavity length [m]
ls = 56.0 # Signal recycling cavity length


n0=1.45 # refractive indx of fused sillica
Ritm=-1934./n0 # ROC of ITM seen from mich side
Rsr3=36. # ROC of SR3

rx=-1.0 # reflection of ITMX 
ry=-1.0
tp=np.sqrt(0.031)
rp=-np.sqrt(1.-tp**2.)
ts=np.sqrt(0.37)
rs=-np.sqrt(1.-ts**2.)

freq1=9099451
freq2=45497255

rM1=np.cos(lsch*2.*np.pi*freq1/c)
rM2=np.cos(lsch*2.*np.pi*freq2/c)
tM1=np.sin(lsch*2.*np.pi*freq1/c)
tM2=np.sin(lsch*2.*np.pi*freq2/c)
rsm1=(rM1-rs)/(1.-rs*rM1)
tsm1=(ts*tM1)/(1.-rs*rM1)#np.sqrt(1.-rsm1**2.)
rsm2=(rM2+rs)/(1.+rs*rM2)
tsm2=(ts*tM2)/(1.+rs*rM2)#np.sqrt(1.-rsm2**2.

class SideBand:
    def __init__(self):
        pass
        
    def C(self, dRoc):
        """
        curvature mismatch matrix
        [00, 10, 20]' = C*[00, 10, 20]'
        """
        #C=np.array([\
        #    [1.-1j*dRoc, 0., -1j*dRoc/np.sqrt(2)],\
        #    [0., 1.-2j*dRoc, 0.],\
        #    [-1j*dRoc/np.sqrt(2), 0., 1.-3j*dRoc]\
        #    ], dtype=np.complex)
        C=np.array([\
            [1., 0., -1j*dRoc/np.sqrt(2)],\
            [0., 1-1j*dRoc, 0.],\
            [-1j*dRoc/np.sqrt(2), 0., 1-2j*dRoc]\
            ], dtype=np.complex)
        #C=np.array([\
        #    [1., 0., -1j*dRoc/np.sqrt(2)*0.],\
        #    [0., 1-1j*dRoc, 0.],\
        #    [-1j*dRoc/np.sqrt(2)*0., 0., 1-2j*dRoc]\
        #    ], dtype=np.complex)
        return C
        
    def M(self, Theta):
        """
        tilting matrix
        [00, 10, 20]'=M*[00, 10, 20]'
        """
        M=np.array([\
            [1., -2j*Theta, 0.],\
            [-2j*Theta, 1., -np.sqrt(8.)*1j*Theta],\
            [0., -np.sqrt(8.)*1j*Theta, 1.]\
            ], dtype=np.complex)
        return M
        
    def t_bs(self, omega, dRocx=0., dRocy=0., Th_BS=0.):
        """
        trans. matrix for bs
        independent of incident direction
        """
        t_bs=-0.5j*\
            (rx*np.dot(self.M(-Th_BS), self.C(dRocx))*np.exp(1j*omega*lsch/c)\
            -ry*np.dot(self.C(dRocy), self.M(Th_BS))*np.exp(-1j*omega*lsch/c))
        return t_bs
        
    def r_bs(self, omega, dRocx=0., dRocy=0., Th_BS=0.):
        """
        ref. matrix for bs
        from PRCL to SRCL
        r_bs>0 as rx=ry=-1
        """
        r_bs=-0.5*\
            (rx*self.C(dRocx)*np.exp(1j*omega*lsch/c)\
            +ry*self.C(dRocy)*np.exp(-1j*omega*lsch/c))
        return r_bs
        
    def r_bs2(self, omega, dRocx=0., dRocy=0., Th_BS=0.):
        """
        ref. matrix for bs
        from SRCL to PRCL
        r_bs2<0 as rx=ry=-1
        """
        r_bs2=0.5*\
            (rx*self.C(dRocx)*np.exp(1j*omega*lsch/c)\
            +ry*self.C(dRocy)*np.exp(-1j*omega*lsch/c))
        #r_bs2=-self.r_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        return r_bs2
        
    def Psrcl(self, omega, etaS=-0.332):
        """
        single trip phase evolution in SRCL
        omega: angular mod. freq 
        eta: guoy phase shift
        
        ideally:
        for 45 MHz 00 Psrcl**2. = 1
             9 MHz 00 Psrcl**2. = -1
             
        in realistic cannot achieve this simultaneously
        if 0.5 lambda for 9
        have to be 2.5 lambda for 45
        so ...
        """
        Psrcl=np.ones(3, dtype=np.complex)
        
        #ideal case
        #if np.abs(omega)<(2.*np.pi*10.e6): 
        #    # 9 MHz sb.
        #    # 00 anti-reson. in SRCL, single trip has a phase pi/2
        #    Psrcl[0]=1j
        #    Psrcl[1]=1j*np.exp(1j*etaS)
        #    Psrcl[2]=1j*np.exp(2j*etaS)
        #else:
        #    # 45 MHz sb. 
        #    # 00 reson. in SRCL, single trip has a phase pi
        #    Psrcl[0]=-1.
        #    Psrcl[1]=-np.exp(1j*etaS)
        #    Psrcl[2]=-np.exp(2j*etaS)
        
        #realistic
        Psrcl[0]=np.exp(1j*omega*ls/c) 
        Psrcl[1]=np.exp(1j*omega*ls/c + 1j*etaS)
        Psrcl[2]=np.exp(1j*omega*ls/c + 2j*etaS)
        
        Psrcl=np.diag(Psrcl)
        return Psrcl
        
    def Pprcl(self, omega, etaP=-0.436):
        """
        single trip phase evolution in PRCL
        """
        Pprcl=-np.ones(3, dtype=np.complex)
        Pprcl[0]=1j
        Pprcl[1]=1j*np.exp(1j*etaP)
        Pprcl[2]=1j*np.exp(2j*etaP)
        
        #Pprcl[0]=np.exp(1j*omega*lp/c)
        #Pprcl[1]=np.exp(1j*omega*lp/c+1j*etaP)
        #Pprcl[2]=np.exp(1j*omega*lp/c*2j*etaP)
        
        Pprcl=np.diag(Pprcl)
        return Pprcl
        
    def Pdmd(self, eta):
        """
        accounting for the extra gouy phase 
        after transmitted from the cavity
        """
        Pdmd=np.ones(3, dtype=np.complex)
        Pdmd[1]=np.exp(1j*eta)
        Pdmd[2]=np.exp(2j*eta)
        return Pdmd
        
    def g_cav_rm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaP=-0.436):
        """
        from srm to prm
        """
        r_bs=self.r_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        t_bs=self.t_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        Pprcl=self.Pprcl(omega, etaP=etaP)
        
        g_cav=np.dot(rp, Pprcl)
        g_cav=np.dot(Pprcl, g_cav)
        g_cav=np.dot(r_bs, g_cav)
        g_cav=np.eye(3, dtype=np.complex) - g_cav
        g_cav=np.linalg.inv(g_cav)
        g_cav=np.dot(g_cav, t_bs)
        return g_cav
        
    def r_rm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaP=-0.436):
        """
        from srm to prm
        """
        r_bs2=self.r_bs2(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        t_bs=self.t_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        Pprcl=self.Pprcl(omega, etaP=etaP)
        g_rm=self.g_cav_rm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaP=etaP)
        #print g_rm
        
        r_rm=rp*np.dot(Pprcl, g_rm)
        r_rm=np.dot(Pprcl, r_rm)
        r_rm=np.dot(t_bs, r_rm)
        #print r_rm
        r_rm=r_bs2+r_rm
        #print r_bs2
        return r_rm
        
    def t_rm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaP=-0.436):
        r_bs=self.r_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        t_bs=self.t_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        Pprcl=self.Pprcl(omega, etaP=etaP)
        
        t_rm=np.dot(r_bs, Pprcl)
        t_rm=np.dot(Pprcl, t_rm)
        t_rm=np.dot(rp, t_rm)
        t_rm=np.eye(3, dtype=np.complex)-t_rm
        t_rm=np.linalg.inv(t_rm)
        t_rm=np.dot(tp, t_rm)
        t_rm=np.dot(Pprcl, t_rm)
        t_rm=np.dot(t_bs, t_rm)
        return t_rm
        
    def g_cav_sm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332):
        """
        gain matrix of the compound cavity formed by mich + src
        """
        r_bs2=self.r_bs2(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS) # r_bs2<0
        t_bs=self.t_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        Psrcl=self.Psrcl(omega, etaS=etaS)
        r_srm=rs*self.M(Th_SRM)
        g_cav=np.dot(r_srm, Psrcl)
        g_cav=np.dot(Psrcl, g_cav)
        g_cav=np.dot(r_bs2, g_cav)
        g_cav=np.eye(3, dtype=np.complex)-g_cav
        g_cav=np.linalg.inv(g_cav)
        g_cav=np.dot(g_cav, t_bs)
        return g_cav
        
    def t_sm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332):
        """
        trans. matrix for signal-recycled Mich. 
        """
        Psrcl=self.Psrcl(omega, etaS=etaS)
        g_cav=self.g_cav_sm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS)        
        t_sm=ts*np.dot(Psrcl, g_cav)
        return t_sm
        
    def r_sm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332):
        """
        ref. matrix for signal-recycled mich
        """
        r_bs=self.r_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS) # r_bs>0
        t_bs=self.t_bs(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS)
        r_srm=rs*self.M(Th_SRM)
        Psrcl=self.Psrcl(omega, etaS=etaS)
        g_cav=self.g_cav_sm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS)
        
        r_sm=np.dot(Psrcl, g_cav)
        r_sm=np.dot(r_srm, r_sm)
        r_sm=np.dot(Psrcl, r_sm)
        r_sm=np.dot(t_bs, r_sm)
        r_sm=r_bs + r_sm
        return r_sm
    
    def g_cav_dm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332, etaP=-0.436):
        """
        cavity gain (matrix) for dual-recycled Mich cavity
        eta = gouy phase in SRCL
        eta_P=gouy phase in PRCL
        """
        r_sm=self.r_sm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS)
        P=self.Pprcl(omega, etaP)
        
        g_cav=np.dot(r_sm, P)
        g_cav=np.dot(P, g_cav)
        g_cav=np.dot(rp, g_cav)
        g_cav=np.eye(3, dtype=np.complex)-g_cav
        g_cav=np.linalg.inv(g_cav)
        g_cav=np.dot(tp, g_cav)
        return g_cav
        
    def t_dm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332, etaP=-0.436, no_shift=True):
        P=self.Pprcl(omega, etaP)
        g_dm=self.g_cav_dm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS, etaP=etaP)
        t_sm=self.t_sm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS)
        
        t_dm=np.dot(P, g_dm)
        t_dm=np.dot(t_sm, t_dm)
        if no_shift:
            Psrcl=self.Psrcl(omega, etaS)
            P=np.dot(P, Psrcl)
            P=np.linalg.inv(P)
            t_dm=np.dot(P, t_dm)
        return t_dm
        
    def r_dm(self, omega, dRocx=0., dRocy=0., Th_BS=0., Th_SRM=0., etaS=-0.332, etaP=-0.436):
        P=self.Pprcl(omega, etaP=etaP)
        g_dm=self.g_cav_dm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS, etaP=etaP)
        r_sm=self.r_sm(omega, dRocx=dRocx, dRocy=dRocy, Th_BS=Th_BS, Th_SRM=Th_SRM, etaS=etaS)
        
        r_dm=np.dot(P, g_dm)
        r_dm=np.dot(r_sm, r_dm)
        r_dm=np.dot(P, r_dm)
        r_dm=tp*r_dm
        r_dm=-rp*np.eye(3)+r_dm
        return r_dm
        
    def find_dmd_ang(self,E1,E2):
        """
        E1 and E2 both complex scalars, with f1 >= f2
        return dmd angle such that maximizes I-phase (cos)
        """
        ang1=np.angle(E1)
        ang2=np.angle(E2)
        d_ang=ang1-ang2
        return d_ang
        
    def BeatNote(self, E1, E2, dmd_ang):
        """
        Find the beat note between two E-fields 
        E1 and E2 both complex scalars, with f1 >= f2
        dmd_ang is the demodulation angle
        return power in both
        I-phase (cos)
        and Q-phase(sin)
        [P_I, P_Q]
        """
        E2=E2*np.exp(1j*dmd_ang)
        a1=np.real(E1)
        b1=np.imag(E1)
        a2=np.real(E2)
        b2=np.imag(E2)
        P=np.zeros(2)
        P[0]=a1*a2+b1*b2
        P[1]=a1*b2-a2*b1

        return P
        
    def check(self):
        r_rm=self.r_rm(2.*np.pi*freq2, Th_BS=0.01)
        t_rm=self.t_rm(2.*np.pi*freq2, Th_BS=0.01)
        
        print r_rm
        Psrcl=self.Psrcl(2.*np.pi*freq2)
        
        t_as=np.dot(rs, Psrcl)
        t_as=np.dot(Psrcl, t_as)
        t_as=np.dot(r_rm, t_as)
        t_as=np.eye(3, dtype=np.complex)-t_as
        t_as=np.linalg.inv(t_as)
        t_as=np.dot(t_rm, t_as)
        t_as=np.dot(Psrcl, t_as)
        t_as=np.dot(ts, t_as)
        
        r_rm=self.r_rm(2.*np.pi*freq2)
        t_rm=self.t_rm(2.*np.pi*freq2)
        
        M=self.M(0.01)
        print rs*M
        t_as2=np.dot(rs*M, Psrcl)
        t_as2=np.dot(Psrcl, t_as2)
        t_as2=np.dot(r_rm, t_as2)
        t_as2=np.eye(3, dtype=np.complex)-t_as2
        t_as2=np.linalg.inv(t_as2)
        t_as2=np.dot(t_rm, t_as2)
        t_as2=np.dot(Psrcl, t_as2)
        t_as2=np.dot(ts, t_as2)
        print t_as
        print t_as2
        
        t_as3=np.dot(rs, Psrcl)
        t_as3=np.dot(Psrcl, t_as3)
        t_as3=np.dot(r_rm[0,0]*M, t_as2)
        t_as3=np.eye(3, dtype=np.complex)-t_as3
        t_as3=np.linalg.inv(t_as3)
        t_as3=np.dot(t_rm, t_as3)
        t_as3=np.dot(Psrcl, t_as3)
        t_as3=np.dot(ts, t_as3)
        print t_as3
        #r_sm=self.r_sm(2.*np.pi*freq2)
        
        return None

